const Discord = require('discord.js');
const client = new Discord.Client();

var CONFIG = require('./config.json');

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
  if (msg.guild != null && !msg.author.bot) {
    if (msg.content === '!kamer') {
      sendList(msg);
    }
    if (msg.content.startsWith('!join')) {
      let args = msg.content.split(" ");
      if (args.length < 2) {
        //To few arguments
        //List available parties
        var message = "Gebruik het commando \"!join [partijnaam]\" om toe te treden tot een partij.\r\n";
        message += "\tBeschikbare partijen: "+CONFIG.parties_list.join(", ");
        console.log(message);
        msg.reply(message);
      } else {
        let partyToJoin = args[1];
        if (CONFIG.parties_list.includes(partyToJoin)) {
          var notYetAMember = true;
          let arr = msg.member.roles.array();
          arr.forEach(function(role) {
              if (CONFIG.parties_list.includes(role.name)) {
                notYetAMember = false;
              }
          });

          if (notYetAMember) {
            var roleToGive = msg.guild.roles.find(role => role.name == partyToJoin);
            msg.member.addRole(roleToGive);
            
            var customEmoji = client.emojis.find(emoji => emoji.name == partyToJoin);
            if (customEmoji == null) customEmoji = ":flag_be:";
            let partyTag = msg.guild.roles.find(role => role.name == partyToJoin);
            var message = "Welkom bij "+customEmoji+" "+partyTag+"!";
            
            var numberOfPartyMembers = 0;
            let members = msg.guild.members;
            members.forEach(function(member) {
              let found = member.roles.find(role => role.name == partyToJoin);
              if (found != undefined) {
                numberOfPartyMembers++;
              }
            });

            if (numberOfPartyMembers == 0) {
              var secondRoleToGive = msg.guild.roles.find(role => role.name == CONFIG.autojob_first);
              msg.member.addRole(secondRoleToGive);

              let roleTag = msg.guild.roles.find(role => role.name == CONFIG.autojob_first);
              message += "\r\nOmdat je het eerste lid bent van deze partij, ben je ook de "+roleTag;
            }
            
            console.log(message);
            msg.reply(message);

            setTimeout(function(){ sendList(msg); }, 1000);
          } else {
            var message = "Je bent al lid van een partij.\r\n";
            console.log(message);
            msg.reply(message);
          }
        } else {
          var message = "Deze partij ken ik niet.\r\n";
          message += "\tBeschikbare partijen: "+CONFIG.parties_list.join(", ");
          console.log(message);
          msg.reply(message);
        }
      }
    }
  } else if (!msg.author.bot) {
    msg.reply("Sorry. Ik werk niet in privé-berichten.");
  }
});

client.login(CONFIG.token);

function sendList(msg) {
  let channelToMessage = msg.guild.channels.find(ch => ch.name == CONFIG.channel_name_list);

  let prevMessages = channelToMessage.fetchMessages()
  .then(messages => messages.forEach(function(message) {
      message.delete();
  }))
  .catch(console.error);
    
  var numberOfMPS = 0;
  var parties = [];
  var others = [];

  let members = msg.guild.members;
  members.forEach(function(member) {
    var jobString = "";
    // Run thru all the roles of the member
    member.roles.forEach(function(role) {
      //Check if the role is a job
      if (CONFIG.jobs_list.includes(role.name)) {
        //This role is a job
        jobString += " ["+role.name+"]";
      }

      //Check if the role is a party
      if (CONFIG.parties_list.includes(role.name)) {
        //This role is a party
        numberOfMPS++;

        //Check if this party is already known
        if (parties[role.name] == undefined) {
          parties[role.name] = [];
        }

        //Add this member to the party
        parties[role.name].push("<@"+member.user.id+">"+jobString);
      }

      //Check if the role is a seperate item
      if (CONFIG.seperate_list.includes(role.name)) {
         //Check if this role is already known
         if (others[role.name] == undefined) {
          others[role.name] = [];
        }

        //Add this member to the otherrole
        others[role.name].push("<@"+member.user.id+">");
      }
    });
  });

  console.log(parties);

  parties.sort(function(x, y) {
    if (x.length < y.length) {
      return -1;
    }
    if (x.length > y.length) {
      return 1;
    }
    return 0;
  });

  var message = "Lijst van volksvertegenwoordigers:\r\n\r\n";
  Object.keys(parties).forEach(function (partyname) {
    let party = parties[partyname];
    var customEmoji = client.emojis.find(emoji => emoji.name == partyname);
    if (customEmoji == null) customEmoji = ":flag_be:";
    let roleTag = msg.guild.roles.find(role => role.name == partyname);
    let prcnt = parseFloat((party.length/numberOfMPS)*100).toFixed(2);
    message += "\t"+customEmoji+" "+roleTag+" ("+party.length+" / "+numberOfMPS+" = "+prcnt+"%): \r\n";
    party.forEach(function(member) {
      message += "\t\t"+member+"\r\n";
    });
    message += "\r\n";
 });
 message += "Andere personen:\r\n\r\n";
 Object.keys(others).forEach(function (othername) {
  let other = others[othername];
  var customEmoji = client.emojis.find(emoji => emoji.name == othername);
  if (customEmoji == null) customEmoji = ":flag_be:";
  let roleTag = msg.guild.roles.find(role => role.name == othername);
  message += "\t"+customEmoji+" "+roleTag+": \r\n";
  other.forEach(function(member) {
    message += "\t\t"+member+"\r\n";
  });
  message += "\r\n";
});
  console.log(message);
  channelToMessage.send(message);
}